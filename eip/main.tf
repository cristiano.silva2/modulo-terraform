terraform {
  required_version = ">= 0.15.0"
}

resource "aws_eip" "nat_gateway" {
  count = var.num_eip
  vpc   = true

  tags = merge(var.tags,
  {
      Terraform = "NÃO MEXER"
  },)
}