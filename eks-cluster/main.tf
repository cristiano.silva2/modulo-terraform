
terraform {
  required_version = ">= 0.15.0"
}

resource "aws_eks_cluster" "eks_cluster" {
  name     = var.name
  role_arn = var.iam_cluster_arn

  vpc_config {
    security_group_ids = var.cluster_security_groups
    subnet_ids         = var.subnets
  }
  
  tags = merge(var.tags,
  {
    Terraform = "NÃO MEXER"
  },)
}