variable "iam_cluster_arn" {
    type = string
}

variable "cluster_security_groups" {
    type = list(string)
}

variable "subnets" {
    type = list(string)
}

variable "name" {
    type = string
    description = "name do cluster eks"
}

variable "tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. map(`BusinessUnit`,`XYZ`)."
}