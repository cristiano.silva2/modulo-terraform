
terraform {
  required_version = ">= 0.15.0"
}

resource "aws_iam_policy" "policy" {
  name        = var.policy_name
  path        = var.policy_path
  description = var.policy_description

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode(var.policy)
  tags = merge(var.tags,
  {
    Terraform = "NÃO MEXER"
  },)
}