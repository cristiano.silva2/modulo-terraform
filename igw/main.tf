terraform {
  required_version = ">= 0.15.0"
}

resource "aws_internet_gateway" "igw" {
    vpc_id = var.igw_vpc_id
    tags = merge(var.tags,
    {
        Terraform = "NÃO MEXER"
    },)
}