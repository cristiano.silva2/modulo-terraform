terraform {
  required_version = ">= 0.15.0"
}

# Recurso que provisiona o Hosted Zone no Route53
resource "aws_route53_zone" "private" {
  name = var.hosted_name

  vpc {
    vpc_id = var.vpc_id
  }
}
