variable "hosted_name" {
  type = string
  description = ""
}

variable "vpc_id" {
  type = string
  description = "VPC id"
}